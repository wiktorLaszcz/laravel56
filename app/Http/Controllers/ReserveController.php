<?php

namespace App\Http\Controllers;
use App\Reserve;
use App\Resource;
use App\User;
use App\MailController;
use Illuminate\Http\Request;
use App\Providers\AppServiceProvider;
use App\Policies\ReservePolicy;
class ReserveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $reserves = Reserve::paginate(2);
         $this->authorize('index', Reserve::class);
        return view('reserves.index',["reserves" => $reserves]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $resources = Resource::all();
        $users = User::all();
        return view('reserves.create',["resources" => $resources, "users" => $users]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $this->validate($request, [
            'user_name' => 'required',
            'start' => 'required',
            'end' => 'required',
            'resources' => 'required',

        ]);
         $reserve = new Reserve();
         $reserve->user_id = $request->user_name;
         $reserve->date = $request->input("start");
         $reserve->limit = $request->input("end") ;
         $reserve->save();

         foreach ($request->input("resources") as $key ) {
            $resource = Resource::find($key);
            $resource->reserve_id = $reserve->id;
            $resource->save();
        }

        if($request->input("send")== 1){
            return redirect("/my_mail/" . $reserve->id );
        }
         return redirect("/reserves");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $reserve = Reserve::find($id);
        return view('reserves.edit',["reserve" => $reserve]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $reserve = Reserve::find($id);
        return view('reserves.edit',["reserve" => $reserve]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

         $this->validate($request, [
            'username' => 'required',
            'date' => 'required',
            'limit' => 'required',
            'resources' => 'required',

        ]);
        $reserve = Reserve::find($id);
         $reserve->limit = $request->input("limit") ;
         $reserve->save();
         return redirect("/reserves");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $reserve = Reserve::find($id);

        foreach ($reserve->resources as $key ) {
            $resource = Resource::find($key->id);
            $resource->reserve_id = null;
            $resource->save();
        }

       $reserve->delete();

        return redirect("/reserves");
    }
}
