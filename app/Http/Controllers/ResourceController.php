<?php

namespace App\Http\Controllers;
use App\Resource;

use Illuminate\Http\Request;

class ResourceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
     //   $this->authorize('index', ResourceController::class);
        $resources = Resource::paginate(5);
        return view('resources.index',["resources" => $resources]);
    }

    /**
     * Show the form for creating a new resource.

     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create',Resource::class);
        return view('resources.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $this->authorize('create', Resource::class);
         $this->validate($request, [
            'name' => 'required|max:255|min:5',
        ]);
         $resource = new Resource();
         $resource->fill($request->all());
         $resource->save();
         return redirect("/resources");
    }

 /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function saveSession($id)
    {
        $resource = Resource::find($id);
         session(['resourcesSession'=>$resource]);

         return redirect("/resources");
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $this->authorize('update', Resource::class);
        $resource = Resource::find($id);
        return view('resources.edit',["resource" => $resource]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $this->authorize('update', Resource::class);
         $this->validate($request, [
            'name' => 'required|max:255|min:5',
        ]);
         $resource = Resource::find($id);
         $resource->fill($request->all());
         $resource->save();
         return redirect("/resources");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('delete', Resource::class);
        $resource = Resource::find($id);
        $resource->delete();

        return redirect("/resources");    }

}
