<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Resource extends Model
{
    protected $fillable = [
        'name'
    ];

    public function reserve()
    {
       return $this->hasMany('App\Reserve');
    }

     public function users()
    {
        return $this->belongsToMany('App\User');
    }
}
