<?php

namespace App\Providers;

use Laravel\Passport\Passport;

use App\Reserve;
use App\Policies\ReservePolicy;

use App\User;
use App\Policies\UserPolicy;

use App\Resource;
use App\Policies\ResourcePolicy;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
        Reserve::class => ReservePolicy::class,
        User::class => UserPolicy::class,
        Resource::class => ResourcePolicy::class

    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
        Passport::routes();
    }
}
