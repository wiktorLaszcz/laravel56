<?php

namespace App\Policies;

use App\User;
use App\Reserve;
use Illuminate\Auth\Access\HandlesAuthorization;

class ReservePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the reserve.
     *
     * @param  \App\User  $user
     * @param  \App\Reserve  $reserve
     * @return mixed
     */
    public function view(User $user, Reserve $reserve)
    {



    }

    /**
     * Determine whether the user can view the reserve.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function index(User $user)
    {
        if(!$user->rol_id)
        {
            return true;
        }else{
            return redirect("/login");;
        }
    }


    /**
     * Determine whether the user can create reserves.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the reserve.
     *
     * @param  \App\User  $user
     * @param  \App\Reserve  $reserve
     * @return mixed
     */
    public function show(User $user, Reserve $reserve)
    {
       if($user->rol_id == 1)
       {
            return true;
        }else{
            if($reserve->user->name == $user->name){
                return true;
            }
        }
    }



    /**
     * Determine whether the user can update the reserve.
     *
     * @param  \App\User  $user
     * @param  \App\Reserve  $reserve
     * @return mixed
     */
    public function update(User $user, Reserve $reserve)
    {
        //
    }

    /**
     * Determine whether the user can delete the reserve.
     *
     * @param  \App\User  $user
     * @param  \App\Reserve  $reserve
     * @return mixed
     */
    public function delete(User $user, Reserve $reserve)
    {
        //
    }
}
