<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the reserve.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function index(User $user)
    {
        if(!$user->rol_id)
        {
            return true;
        }else{
            return redirect("/login");;
        }
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User  $user
     * @param  \App\User  $model
     * @return mixed
     */
    public function view(User $user, User $model)
    {
        //
    }

    /**
     * Determine whether the user can update the reserve.
     *
     * @param  \App\User  $user
     * @param  \App\Reserve  $reserve
     * @return mixed
     */
    public function show(User $user, User $user2)
    {
       if($user->rol_id == 1)
       {
            return true;
        }else{
            if($user->id == $user2->id){
                return true;
            }
        }
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        if($user->rol_id == 1)
        {
            return true;
        }else{
              return false;

        }
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function update(User $user)
    {
        if($user->rol_id == 1)
        {
            return true;
        }else{
              return false;

        }
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function delete(User $user)
    {
        if($user->rol_id == 1)
        {
            return true;
        }else{
              return false;

        }
    }
}
