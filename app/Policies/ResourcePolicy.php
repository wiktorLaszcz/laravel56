<?php

namespace App\Policies;

use App\User;
use App\Resource;

use Illuminate\Auth\Access\HandlesAuthorization;

class ResourcePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the reserve.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function index(User $user)
    {
        if(!$user->rol_id)
        {
            return true;
        }else{
            return redirect("/login");;
        }
    }

    /**
     * Determine whether the user can view the resource.
     *
     * @param  \App\User  $user
     * @param  \App\Resource  $resource
     * @return mixed
     */
    public function view(User $user, Resource $resource)
    {
        //
    }

    /**
     * Determine whether the user can see the reserve.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function show(User $user)
    {
       if($user->rol_id == 1)
       {
            return true;

        }else{

            return false;

        }
    }

    /**
     * Determine whether the user can create resources.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        if($user->rol_id == 1)
       {
            return true;

        }else{

            return false;

        }
    }

    /**
     * Determine whether the user can update the resource.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function update(User $user)
    {
          if($user->rol_id == 1)
       {
            return true;

        }else{

            return false;

        }
    }

    /**
     * Determine whether the user can delete the resource.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function delete(User $user)
    {
          if($user->rol_id == 1)
       {
            return true;

        }else{

            return false;

        }
    }
}
