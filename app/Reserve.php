<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reserve extends Model
{

    protected $fillable = [
        'id', 'user_name', 'resource_name','date','limit',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function resources()
    {
        return $this->hasMany('App\Resource');
    }
}
