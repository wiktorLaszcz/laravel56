<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/rols', "RolController@index");
Route::get('/resources/session/{id}', "ResourceController@saveSession");

Route::resource('/resources', "ResourceController");
Route::resource('/reserves', "ReserveController");
Route::resource('/users', "UserController");

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/redirect/google', 'SocialAuthController@redirect');
Route::get('/callback/google', 'SocialAuthController@callback');

Route::get('/my_mail/{id}', 'MailController@basket');
