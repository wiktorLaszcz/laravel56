<?php

use Illuminate\Database\Seeder;

class ResourceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('resources')->insert([
            'name' => "typeA",
            'reserve_id' => "1",


        ]);
        DB::table('resources')->insert([
            'name' => "typeB",
             'reserve_id' => "2",

        ]);
        DB::table('resources')->insert([
            'name' => "typeC",
             'reserve_id' => "1",

        ]);
        DB::table('resources')->insert([
            'name' => "typeD",
             'reserve_id' => "1",

        ]);
        DB::table('resources')->insert([
            'name' => "typeE",
            'reserve_id' => "2",


        ]);
        DB::table('resources')->insert([
            'name' => "typeF",
            'reserve_id' => 2,


        ]);
    }
}
