<?php

use Illuminate\Database\Seeder;

class ReserveTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('reserves')->insert([
            'user_id' => 1 ,
            'date' => "2018-02-27",
            'limit' => "2018-02-28",
        ]);

       DB::table('reserves')->insert([
            'user_id' => 2 ,
            'date' => "2018-02-27",
            'limit' => "2018-02-28",
        ]);

    }
}
