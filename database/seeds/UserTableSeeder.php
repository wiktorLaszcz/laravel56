<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => "Usuario Administrador",
            'rol_id' => '1',
            'email' => 'admin@gmail.com',
            'password' =>  bcrypt('usuarioadmin')
        ]);
        DB::table('users')->insert([
            'name' => "Usuario Normal",
            'rol_id' => '2',
            'email' => 'user@gmail.com',
            'password' => bcrypt('usuarionormal')
        ]);
        DB::table('users')->insert([
            'name' => "userC",
            'email' => 'algosd@aaa.aa',
            'password' => bcrypt('aaaa')
        ]);
        DB::table('users')->insert([
            'name' => "userD",
            'rol_id' => '2',
            'email' => 'algsso@aaa.aa',
            'password' => bcrypt('aaaa')
        ]);
          }
}
