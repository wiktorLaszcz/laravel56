<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(ReserveTableSeeder::class);
        $this->call(ResourceTableSeeder::class);
        $this->call(UserResourceSeeder::class);
    }
}
