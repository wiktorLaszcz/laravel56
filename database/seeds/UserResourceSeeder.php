<?php

use Illuminate\Database\Seeder;

class UserResourceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_resource')->insert([
            'user_id' => '2',
            'resource_id' => '3'
        ]);
    }
}
