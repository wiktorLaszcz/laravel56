<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ResourcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('resources', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('reserve_id')->nullable()->unsigned();
            $table->foreign('reserve_id')->references('id')->on('reserves');
            $table->string('name');
            $table->rememberToken();
            $table->timestamps();


        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::dropIfExists('resources');
    }
}
