@extends('layouts.app')

@section('content')


<div class="container">
    <h4>User: {{ $user->name}}</h4>
    <h5>Users reserves:</h5>
    @if(count($user->reserves) == 0)
     Reserves NULL
    @else
    <table>
        <tbody>
            <tr>
                <th>Reserve</th>
                <th>Starts</th>
                <th>Endes</th>

                <th>Options</th>
            </tr>

            @foreach($user->reserves as $reserve)
            <tr>
                  <td style="border: 1.2px #AAF solid;">

                    @foreach($reserve->resources as $resource)
                    {{ $resource->name }}
                     @endforeach
                 </td>
                <td style="border: 0.5px #AAF solid;">{{ $reserve->date }}</td>
                <td style="border: 0.5px #AAF solid;">{{ $reserve->limit }}</td>
                <td >
                  <a href="/reserves/{{ $reserve->id }}/edit"><input type="button" value="Edit"></a>--
                  <form method="post" action="/reserves/{{ $reserve->id }}">
                       {{ csrf_field() }}
                     <input type="hidden" name="_method" value="delete">
                     <input type="submit" value="Delete">
                 </form>
                  </td>
            </tr>
            @endforeach
        </table>
    </table>
    @endif
</div>


@endsection
