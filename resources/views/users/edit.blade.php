@extends('layouts.app')

@section('content')


<div class="container">
    <form action="/users/{{ $user->id }}"  method="post">
        <input type="hidden" name="_method" value="PUT">
        {{ csrf_field() }}
        <label>
            Name
            <input type="text" name="name" value="{{ $user->name }}">
        </label>
        <label>
            Email:
            <input type="text" name="email" value="{{ $user->email }}">
        </label>

        <input type="submit" name="save" value="Save">
        <a href="/users">
            <input type="button" name="back" value="Back">
        </a>
    </form>
</div>
@endsection
