@extends('layouts.app')

@section('content')


<div class="container">
<h1>List</h1>
<table style="border: 2px #99F solid;">
    <tr>
        <th style="border: 2px #AAF solid;">Name</th>
        <th style="border: 2px #AAF solid;"> E-mail</th>
        <th style="border: 2px #AAF solid;"> Rol</th>
        <th style="border: 2px #AAF solid;"> Options</th>
    </tr>

    @foreach($users as $user)
     @can('show', $user)
    <tr style="border: 2px #AAF solid;">
        <td style="border: 2px #AAF solid;">{{ $user->name }}</td>
        <td style="border: 2px #AAF solid;">{{ $user->email }}</td>
        <td style="border: 2px #AAF solid;">{{ $user->rol_id }}</td>
        <td >
            <a href="/users/{{ $user->id }}/edit"><input type="button" value="Edit"></a> -
            <a href="/users/{{ $user->id }}"><input type="button" value="Show"></a>
            <form method="post" action="/users/{{ $user->id }}">
                       {{ csrf_field() }}
                     <input type="hidden" name="_method" value="delete">
                     <input type="submit" value="Delete">
            </form>
        </td>
    </tr>
    @endcan
    @endforeach


</table>
{{ $users->links() }}

</div>

@endsection
