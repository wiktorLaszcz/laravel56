@extends('layouts.app')

@section('content')

<div class="container">


    <a href="/reserves/create">
        <input type="button" name="new" value="New">
    </a>
<h1>List</h1>

<table style="border: 2px #99F solid;">
    <tr>
        <th style="border: 2px #AAF solid;">User Name</th>
        <th style="border: 2px #AAF solid;">Resource</th>
        <th style="border: 2px #AAF solid;">Date</th>
        <th style="border: 2px #AAF solid;">Limit</th>
        <th style="border: 2px #AAF solid;">Options</th>
    </tr>

    @foreach($reserves as $reserve)
    @can('show',$reserve)
    <tr style="border: 2px #AAF solid;">
        <td style="border: 1.2px #AAF solid;">{{ $reserve->user->name }}</td>
        <td style="border: 1.2px #AAF solid;">

             @foreach($reserve->resources as $resource)
              {{ $resource->name }}
             @endforeach
        </td>
        <td style="border: 0.5px #AAF solid;">{{ $reserve->date }}</td>
        <td style="border: 0.5px #AAF solid;">{{ $reserve->limit }}</td>

        <td >
            <a href="/my_mail/{{ $reserve->id }}"><input type="button" value="Send"></a>--
            <a href="/reserves/{{ $reserve->id }}/edit"><input type="button" value="Edit"></a>--
            <form method="post" action="/reserves/{{ $reserve->id }}">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="delete">

                <input type="submit" value="Delete">
            </form>
        </td>
    </tr>
    @endcan
    @endforeach
</table>
{{ $reserves->links() }}

</div>
@endsection
