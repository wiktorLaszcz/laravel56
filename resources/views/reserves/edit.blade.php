@extends('layouts.app')

@section('content')


<div class="container">
    <form method="post" action="/reserves/{{ $reserve->id }}" readonly="readonly">
        {{ csrf_field() }}

        <input type="hidden" name="_method" value="put">
        <label>
            User Name
            <input type="text" name="username" value="{{ $reserve->user->name }}" readonly="readonly">
        </label><br>
        <label>
            Resouces:
            @foreach($reserve->resources as $resource)
                   <input type="text" name="resources" value="{{ $resource->name }}" readonly="readonly">
            @endforeach
        </label><br>
        <label>
            Starts
            <input type="text" name="date" value="{{ $reserve->date }}" readonly="readonly">
        </label><br>
        <label>
            Ends
            <input type="text" name="limit" value="{{ $reserve->limit }}">
        </label><br>

        <input type="submit" name="save" value="Save">
        <a href="/reserves">
            <input type="button" name="back" value="Back">
        </a>
    </form>
</div>
@endsection
