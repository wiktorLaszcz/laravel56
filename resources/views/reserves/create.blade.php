@extends('layouts.app')

@section('content')


<div class="container">
    Crear Reservas
    <form action="/reserves"  method="post">
        {{ csrf_field() }}
        <label>
            User Name:
            <select name="user_name">
                @foreach($users as $user)
                @can('show', $user)

                 <option value="{{$user->id}}"> {{$user->name}}</option>
                @endcan
               @endforeach
            </select>
        </label><br>

        <label>
            Resources:
            <br>

            @foreach($resources as $resource)

                <?php if ($resource->reserve_id == null) {
                ?>
                <input type="checkbox" name="resources[]" value="{{$resource->id}}"> {{$resource->name}}<br>
                <?php
                  }
                ?>
            @endforeach
        </label>
        <br>

        <label>
            Starts:
            <input type="text" name="start">
        </label>

        <label>
            Ends:
            <input type="text" name="end">
        </label>

         <label>
            Send:
            <input type="checkbox" name="send" value="1">
        </label>



        <input type="submit" name="save" value="Save">
        <a href="/reserves">
            <input type="button" name="back" value="Back">
        </a>
    </form>
</div>

@endsection
