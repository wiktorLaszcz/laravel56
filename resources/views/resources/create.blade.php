@extends('layouts.app')

@section('content')


<div class="container">
    <form action="/resources"  method="post">
        {{ csrf_field() }}
        <input type="text" name="name" >
        <input type="submit" name="save" value="Save">
        <a href="/resources">
            <input type="button" name="back" value="Back">
        </a>
    </form>
</div>
@endsection
