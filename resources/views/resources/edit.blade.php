@extends('layouts.app')

@section('content')


<div class="container">
    <form method="post" action="/resources/{{ $resource->id }}">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="put">
        <input type="text" name="name" value="{{ $resource->name }}">
        <input type="submit" name="save" value="Save">
        <a href="/resources">
            <input type="button" name="back" value="Back">
        </a>
    </form>
</div>
@endsection
