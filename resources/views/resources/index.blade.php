@extends('layouts.app')

@section('content')


<div class="container">
<h1>List</h1>
<table style="border: 2px #99F solid;">
    <tr>
        <th style="border: 2px #AAF solid;">Name</th>
        <!--<th style="border: 2px #AAF solid;">Description</th> -->
         @can("show", App\Resource::class)
        <th style="border: 2px #AAF solid;"></th>
        @endcan
    </tr>

    @foreach($resources as $resource)
    <tr style="border: 2px #AAF solid;">
        <td style="border: 2px #AAF solid;">{{ $resource->name }}</td>
        @can('show',  App\Resource::class)
        <td ><a href="/resources/{{ $resource->id }}/edit"><input type="button" value="Edit"></a></td>
        <td>
            <form method="post" action="/resources/{{ $resource->id }}">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="delete">

                <input type="submit" value="Delete">
            </form>
        </td>
        @endcan
        <td>
            <a href="/resources/session/{{ $resource->id }}"><input type="button" value="save"></a>
        </td>
    </tr>
    @endforeach
</table>
{{ $resources->links() }}
@can("show",  App\Resource::class)
<a href="/resources/create">New</a>
@endcan
</div>
@endsection
