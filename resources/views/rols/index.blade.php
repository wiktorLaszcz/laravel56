
<div class="container">
<h1>List</h1>
<table style="border: 2px #99F solid;">
    <tr>
        <th style="border: 2px #AAF solid;">Name</th>
        <th style="border: 2px #AAF solid;"></th>
    </tr>

    @foreach($rols as $rol)
    <tr style="border: 2px #AAF solid;">
        <td style="border: 2px #AAF solid;">{{ $rol->name }}</td>
        <td style="border: 2px #AAF solid; display: flex;">
            <form method="post" action="/articles/{{ $rol->id }}">
                 {{ csrf_field() }}
                 <input type="hidden" name="_method" value="delete">
                <input type="submit" name="Delete" value="Delete">
            </form>
            -<a href="/rols/{{ $rol->id }}"><input type="submit" name="Show" value="Show"></a>-
            <a href="/rols/{{ $rol->id }}/edit"><input type="submit" name="Edit" value="Edit"></a>
        </td>
    </tr>
    @endforeach
</table>

<a href="/rols/create">New</a>
</div>

