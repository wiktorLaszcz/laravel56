<?php

namespace Tests\Feature;

use Tests\TestCase;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ResourceTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testResourceView()
    {
        $this->visit('/resources')
            ->see('List')
            ->see('Name')
            ->assertTrue(true);

        $this->visit('/resources')
            ->see('Login')
            ->click('Login')
            ->seePageIs('/login');

    }
}
